import React, { Component } from 'react';
import Produtos from '../../../assets/pessoal_search.json'

import ProductCard from './ProductCard'

class ProductContainer extends Component {
     //COMPUTED
     
     getMinMaxPrice () {
          // função para limitar slide de intervalo de preço
          let  arr = Produtos.map(prod => prod.items[0].sellers[0].commertialOffer.Price)
               
          let  max = Math.max(...arr),
               min = Math.min(...arr)          
          return {max, min}
     }
     /* #region Array dos filtros ativos por grupo */
     colorFilters () {
          let f = this.props.activeFilters().filter(elem => elem.key === "colors")
          return f.map(c => c.id)
     }
     colectionFilters () {
          let f = this.props.activeFilters().filter(elem => elem.key === "collections")
          return f.map(c => c.id)
     }
     categoryFilters () {
          let f = this.props.activeFilters().filter(elem => elem.key === "categories")
          return f.map(c => {
               if (c.hasOwnProperty('pai')) return `/${c.pai}/${c.id}/`
               else return `/${c.id}/`
          })
     }
     priceFilters () {
          let f = this.props.activeFilters().filter(elem => elem.key === "priceRange")

          let  min = !!f.find(p => p.id === 'inicio')  ? f.find(p => p.id === 'inicio').value  : null
          let  max = !!f.find(p => p.id === 'fim')     ? f.find(p => p.id === 'fim').value     : null
          
          return { min, max }
     }
     /* #endregion Array dos filtros ativos por grupo */

     filterSpecsArray (prods, specs, filters) {
          return prods.filter(function (e) {
               let itHasTheSpec = false

               e[specs].forEach(elem => {
                    if (this.includes(elem)) itHasTheSpec = true
               })
               return itHasTheSpec
          }, filters);
     }
     filterSpecsObject (prods, specs, filters) {
          
          return prods.filter(function (e) {
               let itHasTheSpec = false
              
               for (const elem in e[specs]) {
                    
                    if (e[specs].hasOwnProperty(elem)) {
                         if (this.includes(elem)) itHasTheSpec = true  
                    }
               }
               return itHasTheSpec
          }, filters);
     }
     filterSpecsPrice (prods, filters) {
          let min = filters.min ? filters.min : this.getMinMaxPrice().min
          let max = filters.max ? filters.max : this.getMinMaxPrice().max
          
          if ( typeof min === 'string') min = Number(min.split('R$')[1].replace(/,/, '.'))
          if ( typeof max === 'string') max = Number(max.split('R$')[1].replace(/,/, '.'))

          let inBetween = e => e.items[0].sellers[0].commertialOffer.Price >= min
                          &&   e.items[0].sellers[0].commertialOffer.Price <= max
          
          return prods.filter(inBetween)
     }

     getUnique (geral) {
          const unique = Array.from(new Set(geral.map(s => s.productId)))
               .map(id => {
                    let d = geral.find(e => e.productId === id)
                    return d
               })
          
          return unique
     }

     productGrupedByFilters = () => {
          let poductsByFiltersType = []

          // console.log(this.props.activeFilters())
          //Chegando se existem filtros ativos
          if (this.props.activeFilters().length > 0) {
               
               //Filtrando as coleções [obj]
               if (this.colectionFilters().length > 0) {
                    poductsByFiltersType.push(this.filterSpecsObject(Produtos, 'searchableClusters', this.colectionFilters()))
               }

               //Filtrando as cores [arr]
               if (this.colorFilters().length > 0) {
                    poductsByFiltersType.push(this.filterSpecsArray(Produtos, 'Cor', this.colorFilters()))
               }

               //Filtrando as Categorias [arr]
               if (this.categoryFilters().length > 0) {
                    poductsByFiltersType.push(this.filterSpecsArray(Produtos, 'categoriesIds', this.categoryFilters()))
               }

               //Filtrando as Preço [arr]
               if (!!this.priceFilters().min || !!this.priceFilters().max) {
                    poductsByFiltersType.push(this.filterSpecsPrice(Produtos, this.priceFilters()))
               }
          }
          
          return poductsByFiltersType
     }

     filterIntersections = (filter1, filter2) => {
          return filter1.filter(function (e) {
                    return this.includes(e.productId)
          }, filter2.map(c => c.productId))
     }
     filteredProducts = () => {
          let filters = this.productGrupedByFilters()
          let resultProducts = filters[0]
     
          for (let i = 0; i < filters.length - 1; i++) {
               resultProducts = this.filterIntersections(resultProducts, filters[i + 1])
          }
          
          if (this.props.activeFilters().length > 0) return resultProducts
          else return Produtos
     }

     render() { 
          return ( 
               <React.Fragment>
               <p>Total de Produtos: {this.filteredProducts().length}</p>

               <div className="product__container">
                    {this.filteredProducts().map(product =>
                         <ProductCard product={product} key={product.productId}/>
                    )}
               </div>
               </React.Fragment>
           );
     }
}
 
export default ProductContainer;