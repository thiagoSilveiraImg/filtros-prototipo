import React from 'react';
import './ProductCard.css'


const ProductCard = (props) => {
     
     function formatPrice (amount) {
          return amount.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })
     }
     function finalPrice () {
          let price = props.product.items[0].sellers[0].commertialOffer.Price
          return formatPrice(price)
     }
     function inicialPrice () {
          let price = props.product.items[0].sellers[0].commertialOffer.ListPrice
          return formatPrice(price)
     }

     return ( 
          <div className="product__card">
               
               <div className="product__img--wrapper">
                    <img src={props.product.items[0].images[0].imageUrl} alt="" />
               </div>
               <div className="product__card--content">
                    <h6>{props.product.productName}</h6>
                    <div className="content--rate">
                         <i className="fas fa-star"></i>
                         <i className="fas fa-star"></i>
                         <i className="fas fa-star"></i>
                         <i className="fas fa-star" style={{opacity: 0.3}}></i>
                         <i className="fas fa-star" style={{opacity: 0.3}}></i>
                         <span>(90)</span>
                    </div>
                    <div className="content--price">
                         <span className="price--to">{finalPrice()}</span>
                         <span v-if="productHasDiscount" className="price--from">{inicialPrice()}</span>
                    </div>
               </div>
               <button>Adicionar a sacola</button>
          </div>
      );
}
 
export default ProductCard;