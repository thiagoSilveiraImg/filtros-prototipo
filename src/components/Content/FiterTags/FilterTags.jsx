import React from 'react';
import './FilterTags.css'

const FilterTags = (props) => {

     function formatPrice (amount) {
          return amount.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })
     }     

     return (
          <div className="pill--wrapper">
                         
               <div className="pill--wrapper">{props.activeFilters().map( activeFilter =>
                    <div className="pill" key={activeFilter.id}>
                         <div className="pill--content">
                              {activeFilter.key === 'colors'     && <span>{activeFilter.id}</span>}
                              {activeFilter.key !== 'colors'     && <span>{activeFilter.label}</span>}
                              {activeFilter.key === 'priceRange' && <span>{formatPrice(activeFilter.value)}</span>}
                         </div>
                         <button onClick={() => { props.handleCloseClick(activeFilter); }}><i className="fas fa-times"></i></button>
                    </div>)}
               </div>

          </div>
      );
}
 
export default FilterTags;


