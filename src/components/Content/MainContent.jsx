import React from 'react';
import './MainContent.css'
import Produtos from '../../assets/pessoal_search.json'

import FilterContainer from './FIlterContainer/FilterContainer'
import ProductContainer from './Product/ProductContainer'
import FilterTags from './FiterTags/FilterTags'

class MainContent extends React.Component {
     constructor (props) {
          super(props)
          this.state = {
               collections: [    
                    { id: "313", label: "Presentes Namorado",    value: false},
                    { id: "318", label: "Presentes Namorada",    value: false},
                    { id: "406", label: "Unicornio",             value: false},
                    { id: "583", label: "Preguiça",              value: false},
                    { id: "317", label: "Presenta Mãe",          value: false},
                    { id: "316", label: "Presentes Feminino",    value: false},
                    { id: "567", label: "Pantufas",              value: false},
                    { id: "416", label: "Harry Potter",          value: false},
                    { id: "536", label: "Sereia",                value: false},
                    { id: "310", label: "Presentes Pai",         value: false}
               ],
               colors: [
                    { id: 'vermelho',   label: 'DB5151', value: false },
                    { id: 'rosa',       label: 'E161A5', value: false },
                    { id: 'azul',       label: '3B89EE', value: false },
                    { id: 'laranja',    label: 'F57B1D', value: false },
                    { id: 'verde',      label: '5BA75B', value: false },
                    { id: 'roxo',       label: '6956CF', value: false },
                    { id: 'amarelo',    label: 'EFBA29', value: false },
                    { id: 'branco',     label: 'E6E6E6', value: false },
                    { id: 'preto',      label: '212121', value: false }
               ],
               priceRange: [
                    {    id: 'inicio', label: 'à partir de:',
                         value: this.getMinMaxPrice().min },
                    {    id: 'fim',    label: 'até:',
                         value: this.getMinMaxPrice().max }
               ],
               categories: [
                    {id:"3",
                    label:"Pessoal",
                    values: [
                         {pai: "3", id:"43",  label: "Bolsas",             value: false},
                         {pai: "3", id:"50",  label: "Jogos",              value: false},
                         {pai: "3", id:"45",  label: "Diversos Pessoal",   value: false},
                         {pai: "3", id:"47",  label: "Mochilas",           value: false},
                         {pai: "3", id:"184", label: "Bolsas Térmicas",    value: false},
                         {pai: "3", id:"44",  label: "Produtos de Beleza", value: false},
                         {pai: "3", id:"49",  label: "Vestuario",          value: false}] },{ 
                    id:"67",
                    label:"Visto",
                    values:[
                         {pai: "67", id:"68", label:"Bolsas",      value: false},
                         {pai: "67", id:"71", label:"Cosméticos",  value: false}]
                    }
               ]
          }
     }

     activeFilters = () => {
          let geral = []
          let filters = Object.assign({}, this.state)
          
          for (const key in filters) {
               let trueElements = []
               let elements = []
               
               if (filters.hasOwnProperty(key)) {
                    elements = filters[key]
                    
                    //vereificando se existe array dentro de values
                    if (elements[0].hasOwnProperty('values')) {
                         elements = elements.flatMap(item => item.values)
                    }

                    if (key === 'priceRange') {
                         // elements === [{...}, {...}]
                         if (elements[0].value !== this.getMinMaxPrice().min) trueElements.push(elements[0])
                         if (elements[1].value !== this.getMinMaxPrice().max) trueElements.push(elements[1])
                         
                    } else { 
                         // Propriedades boleanas
                         // filtrando apenas values positivos
                         trueElements = elements.filter(elem => !!elem.value)
                    }
                    //Adicionando Chave para idenficação
                    trueElements.map(elem => elem.key = key )

                    geral.push(...trueElements)
               }
          }
          
          return geral
     }

     handleOptionClick = (filterName, input) => {
          const filter = this.state[filterName].slice()

          //Checando se é valor Num
          if (filterName === 'priceRange') {
               // option = filter.find(f => f.id === input.id)
               // option.value = input.value
               let minFilter = filter.find(f => f.id === 'inicio')
               let maxFilter = filter.find(f => f.id === 'fim')

               minFilter.value = input.min
               maxFilter.value = input.max
          }
         
          this.setState({[filterName]: filter})
     }

     excludeFilter = (filter) => {
          let  key = filter.key,
               toExclude,
               arr = []
          
          if (key === 'categories') arr = this.state.categories.find(e => e.id === filter.pai).values
          else arr = this.state[key]

          toExclude = arr.find(item => item.id === filter.id)

          if (!!toExclude) { //retirar depois de resolver categorias
               if (key === 'priceRange') {
                    toExclude.value = toExclude.id === 'inicio' ? 
                    this.getMinMaxPrice().min : this.getMinMaxPrice().max
               }
               else toExclude.value = false

               this.forceUpdate()
               
          } else alert('exclua no filtro')
          
     }

     getMinMaxPrice = () => {
          // função para limitar slide de intervalo de preço
          let  arr = Produtos.map(prod => prod.items[0].sellers[0].commertialOffer.Price)
               
          let  min = Math.floor(Math.min(...arr)),
               max = Math.ceil(Math.max(...arr))

          return {max, min}
     }

     render () {
          return (
               <div id="main__content">
                    <section className="page__info">
                         <p className="bread__crumb">
                              <a href="/">Home</a> / <a href="/">Categoria</a> / <a href="/">Categoria</a>
                         </p>
                         <h2>Nome da Categoria</h2>
                    </section>

                    <FilterContainer 
                         handleOptionClick = {this.handleOptionClick}
                         minMaxPrice =       { this.getMinMaxPrice }
                         {...this.state}
                    />

                    <FilterTags
                         activeFilters={this.activeFilters}
                         handleCloseClick={this.excludeFilter}
                    />

                    <ProductContainer activeFilters={this.activeFilters}/>
                    
               </div>
          );
     }

}

export default MainContent