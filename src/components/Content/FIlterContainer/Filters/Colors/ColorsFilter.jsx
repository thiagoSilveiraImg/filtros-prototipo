import React from 'react';
import './ColorFilter.css'

import FilterBase from '../../FilterBase'

const ColorFilter = (props) => {

     function handleClick (element) {
          element.value = !element.value
          return props.onOptionClick('colors', element);
     }
     function valueHasChange (arr) {
          let reducer = (acc, cur) =>  acc || cur.value
          
          if (arr[0].hasOwnProperty('values')) {
               arr = arr.flatMap(item => item.values)
          }
          
          return arr.reduce(reducer, 0)
     }

     return ( 
          <FilterBase
               filterName="Cores"
               onHeadClick={props.onHeadClick}
               activeFilter={props.activeFilter}
               isSelected={valueHasChange(props.data)}
          >
               <div className="flex-wrapper">
                    { props.data.map( element =>
                         <div className="flex-item"
                              key={element.id}
                              style={{backgroundColor: '#' + element.label}}
                              onClick={() => handleClick(element)}
                              >
                              {element.value && <i className="fas fa-check"></i>}
                         </div>
                    )}
               </div>
          </FilterBase>
     );
}
 
export default ColorFilter;