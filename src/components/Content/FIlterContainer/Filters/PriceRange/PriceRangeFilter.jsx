import React, {Component} from 'react';

import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css'

import './PriceRangeFilter.css'
import FilterBase from '../../FilterBase'

class PriceRangeFilter extends Component {
     constructor(props) {
          super(props);
          this.state = { 
               // value: { min: 20, max: 300 }
           }
     }

     formatPrice (amount) {
          return amount.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })
     }   

     valueHasChange (arr) {
          if (
               arr[0].value !== this.props.minMax().min ||  //inicio
               arr[1].value !== this.props.minMax().max     //fim
          )    return true
          else return false
     }

     getMinMax = () => {
          let min = this.props.data.find(f => f.id === 'inicio').value
          let max = this.props.data.find(f => f.id === 'fim').value
          
          return {min , max}
     }

     handleRangeChange = value => {
          
          this.props.onRangeChange('priceRange', value)
     }

     render() {
          return ( 
          <FilterBase filterName="Preço"
               onHeadClick={this.props.onHeadClick}
               activeFilter={this.props.activeFilter}
               isSelected={this.valueHasChange(this.props.data)}
          >    
               <div className="range__slider--wrapper">
                    <div className="range__slider--head">
                         <div>
                              <p className="range__slider--head--label">de:</p>
                              <p className="range__slider--head--value">
                                   {this.formatPrice(this.getMinMax().min)}
                              </p>
                         </div>
                         <div style={{textAlign: "right"}}>
                              <p className="range__slider--head--label">até:</p>
                              <p className="range__slider--head--value">
                                   {this.formatPrice(this.getMinMax().max)}
                              </p>
                         </div>
                    </div>
                    <InputRange
                         minValue={this.props.minMax().min} maxValue={this.props.minMax().max} //Buscar intervalo de preços dos produtos
                         value={this.getMinMax()}
                         formatLabel={() => ``}
                         onChange={value => this.handleRangeChange(value)} />
               </div>
          </FilterBase>
               );
     }
}
 
export default PriceRangeFilter;

