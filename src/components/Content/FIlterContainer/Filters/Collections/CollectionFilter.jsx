import React from 'react'

import FilterBase from '../../FilterBase'
import './CollectionFilter.css'

const CollectionFilter = props => {

     function handleClick (element) {
          element.value = !element.value
          
          return props.onOptionClick('collections', element);
     }

     function valueHasChange (arr) {
          let reducer = (acc, cur) =>  acc || cur.value
          
          if (arr[0].hasOwnProperty('values')) {
               arr = arr.flatMap(item => item.values)
          }
          
          return arr.reduce(reducer, 0)
     }

     return (
          <FilterBase
               filterName="Coleções"
               onHeadClick={props.onHeadClick}
               activeFilter={props.activeFilter}
               isSelected={valueHasChange(props.data)}
          >
               <ul>
                    { props.data.map( element => 
                         // <li key={c.id}>{c.label}</li>
                         <li key={element.id}>
                              <div htmlFor={element.id} onClick={() => handleClick(element)}>
                                   <i className={ element.value ? 'fas fa-check-square' : 'far fa-square'}></i>
                                   <span>{element.label}</span>
                              </div>
                         </li>
                    )}
               </ul>
          </FilterBase>
     );
}

export default CollectionFilter


