import React from 'react'

import FilterBase from '../../FilterBase'
import './CategoriesFilter.css'

const CategoriesFilter = props => {

     function handleClick(element) {
          element.value = !element.value
          return props.onOptionClick('categories', element);
     }

     function valueHasChange (arr) {
          let reducer = (acc, cur) =>  acc || cur.value
          
          if (arr[0].hasOwnProperty('values')) {
               arr = arr.flatMap(item => item.values)
          }
          
          return arr.reduce(reducer, 0)
     }
     
     return (
     <FilterBase
          filterName="Categorias"
          onHeadClick={props.onHeadClick}
          activeFilter={props.activeFilter}
          isSelected={valueHasChange(props.data)}
     >
          
          <div className="categories--wrapper"> {props.data.map(element => 
               
               <ul key={element.id}>
                    <p>{element.label}</p> {element.values.map(item =>
                    <li key={item.id}>
                         <div onClick={() => handleClick(item)}>
                              <i className={ item.value ? 'fas fa-check-square' : 'far fa-square'}></i>
                              <span>{item.label}</span>
                         </div>
                    </li>)}
               </ul>)}
          </div>
     </FilterBase>);
}
 
export default CategoriesFilter;
