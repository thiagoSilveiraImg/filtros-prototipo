import React from 'react'
import PropTypes from 'prop-types';

import './FilterBase.css'

class FilterBase extends React.Component {

     constructor(props) {
          super(props);
      
          this.setWrapperRef = this.setWrapperRef.bind(this);
          this.handleClickOutside = this.handleClickOutside.bind(this);
     }
     
     // #region OUTSIDE CLICK
     componentDidMount() {
          document.addEventListener('mousedown', this.handleClickOutside);
     }
      
     componentWillUnmount() {
          document.removeEventListener('mousedown', this.handleClickOutside);
     }
      
     // Set the wrapper ref
     setWrapperRef(node) { this.wrapperRef = node;}

     // Alert if clicked on outside of element
     handleClickOutside(event) {
          if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
               // console.log(event);
               
               let openDialog = document.querySelector('.filter--isOpen')
               
			if (openDialog) {
                    if (!event.path.includes(openDialog)) { //n foi clidado no botão
                         //Desativa Filtro aberto
                         this.props.onHeadClick(null)
				}
			}
          }
     }
     // #endregion OUTSIDE CLICK

     handleClickInside = () => {
          this.props.onHeadClick(this.props.filterName)
     }

     openDialog = () => {
          let activeFilter = this.props.activeFilter
          
          if (activeFilter === this.props.filterName) return true
          else return false
     }

     filterBaseClass() {
          return ( 
               'filter '
               + (this.openDialog()     ? 'filter--isOpen '      : '')
               + (this.props.isSelected ? 'filter--isSelected '   : '')
               + (this.props.filterName === 'Preço' ? 'price__filter' : '')
          );
     }

     render () {
          return (
               <div ref={this.setWrapperRef} className={this.filterBaseClass()}>
                    <div className="filter--head" onClick={this.handleClickInside} >
                         <p>{this.props.filterName}</p>
                         <i className={'fas ' + (this.openDialog() ? 'fa-chevron-up' : 'fa-chevron-down')}></i>
                    </div>
                    { this.openDialog() && <div className="filter--body"> {this.props.children} </div> }
               </div>
          );
     }

     
}

FilterBase.propTypes = {
     children: PropTypes.element.isRequired,
};
export default FilterBase