import React from 'react'
import './FilterContainer.css'

import ColletionFilter from './Filters/Collections/CollectionFilter'
import ColorFilter from './Filters/Colors/ColorsFilter'
import PriceRangeFilter from './Filters/PriceRange/PriceRangeFilter'
import CategoriesFilter from './Filters/Categories/CategoriesFilter'

class FilterContainer extends React.Component {

     state = { activeFilter: null }

     handleHeadClick = (filterName) => {
          if (this.state.activeFilter === filterName) this.setState({activeFilter: null})
          else this.setState({activeFilter: filterName})
     }

     priceMinMax

     render () {
          return (
               <section id="filter__container">
                    <div className="filter--wrapper">
                         <ColletionFilter
                              data           = {this.props.collections}
                              onOptionClick  = {this.props.handleOptionClick}
                              onHeadClick    = {this.handleHeadClick}
                              activeFilter   = {this.state.activeFilter}
                         />
                         <ColorFilter
                              data           = {this.props.colors}
                              onOptionClick  = {this.props.handleOptionClick}
                              onHeadClick    = {this.handleHeadClick}
                              activeFilter   = {this.state.activeFilter}
                         />
                         <PriceRangeFilter
                              data           = {this.props.priceRange}
                              onRangeChange  = {this.props.handleOptionClick}
                              onHeadClick    = {this.handleHeadClick}
                              activeFilter   = {this.state.activeFilter}
                              minMax         = {this.props.minMaxPrice}
                         />
                         <CategoriesFilter
                              data           = {this.props.categories}
                              onOptionClick  = {this.props.handleOptionClick}
                              onHeadClick    = {this.handleHeadClick}
                              activeFilter   = {this.state.activeFilter}
                         />
                    </div>
                    
               </section>
          );
     }
}

export default FilterContainer