import React from 'react';
import './App.css';

import NavBar from './components/NavBar/navBar'
import MainContent from "./components/Content/MainContent";

function App() {
     return (
          <div className="App">
               <NavBar />
               <MainContent />
          </div>
     );
}
     
export default App;
     